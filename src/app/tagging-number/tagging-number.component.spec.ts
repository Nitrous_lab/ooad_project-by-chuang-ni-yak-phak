import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaggingNumberComponent } from './tagging-number.component';

describe('TaggingNumberComponent', () => {
  let component: TaggingNumberComponent;
  let fixture: ComponentFixture<TaggingNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaggingNumberComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TaggingNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
