import { Component } from '@angular/core';
import { Order } from '../../Interface/Order';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { OrderService } from '../Service/Order/order.service';

@Component({
  selector: 'app-tagging-number',
  templateUrl: './tagging-number.component.html',
  styleUrl: './tagging-number.component.scss'
})
export class TaggingNumberComponent {
  OrderList$: Observable<Order[]>

  constructor(private store : Store<{Orders : Order[]}> , private OrderStore :OrderService){
    this.OrderList$ = this.store.select('Orders')

  }
  ngOnInit(): void {
    this.OrderStore.OrderInShipment()
    
  }

}
