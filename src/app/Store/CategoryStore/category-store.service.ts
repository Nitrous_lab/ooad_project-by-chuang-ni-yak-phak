import { Injectable } from '@angular/core';
import { CategoryService } from '../../Service/Category/category.service';
import { Category } from '../../../Interface/Category';
import { Store } from '@ngrx/store';
import { GetCategory } from '../../State/Category/Category.action';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryStoreService {
  public CartList$ : Observable<Category[]>;
  constructor(private categoryService : CategoryService , private store : Store<{CategoryList : Category[]}>) {
    this.CartList$ = this.store.select('CategoryList')
   }

  getCategory(){
    this.categoryService.getCategory().subscribe(Categorys => this.store.dispatch(GetCategory.getSucces({Categorys})) ) ;
   }
  
}
