import { Injectable } from '@angular/core';
import { Product } from '../../../Interface/Product';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CategoryAction } from '../../State/Cart/Cart.action'
import { FormControl } from '@angular/forms';



@Injectable({
  providedIn: 'root'
})
export class CartStoreService {
  public currentCart$ : Observable<Product[]>
  public CartList : Product[] = [];
  public Total  = 0;

 
  constructor(private store : Store<{currentCart : Product[]}>) {
  this.currentCart$ = this.store.select('currentCart');
  }

  AddToCart(Product : Product){

    if(Product.product_size![0] == undefined)
    return alert("Select Size");
    this.store.dispatch(CategoryAction.addProduct({Product}))
    this.currentCart$.subscribe(Carts => this.CartList = Carts);
    this.sumTotal(Product)
    console.log(this.CartList)

  }

  RemoveToCart(Product : Product){
    this.store.dispatch(CategoryAction.removeProduct({Product}))
    this.currentCart$.subscribe(Carts => {
      this.CartList = Carts;
      console.log(Carts)
      this.Total = 0;
      Carts.forEach((item) => this.Total = this.Total + item.product_price!)
    });
    // this.sumTotal(Product)
    console.log(this.Total)
    
  }
  ClearCart(){
    this.Total = 0;
    this.store.dispatch((CategoryAction.clean({Product :{}})))
  }
  sumTotal(Product : Product){
    this.Total = 0;
    this.CartList.forEach((item) => {return this.Total = this.Total + item.product_price! })


  }
 

}
