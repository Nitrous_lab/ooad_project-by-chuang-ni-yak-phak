import { Injectable } from '@angular/core';
import { ProductService } from '../../Service/Product/product.service';
import { Product } from '../../../Interface/Product';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GetProduct } from '../../State/Product/Product.action'


@Injectable({
  providedIn: 'root'
})
export class ProductStoreService {
  private ProductList$ : Observable<Product[]> ;
  private cuurentProduct$ : Observable<Product>
  ;
  
  constructor(private ProductService : ProductService , private store:Store<{ProductList : Product[],cuurentProduct : Product}>) {
    this.ProductList$ = this.store.select('ProductList');
    this.cuurentProduct$ = this.store.select('cuurentProduct');

   }
   
   getProduct(){
    this.ProductService.getProduct().subscribe(Products => this.store.dispatch(GetProduct.getSucces({Products})));
   }

   getProductByCategoryId(id :number){
    this.ProductService.getProductByCategory(id).subscribe(Products => this.store.dispatch(GetProduct.getSucces({Products})))
   }

   getProductById(id :number){
    this.ProductService.getProductById(id).subscribe(Product => this.store.dispatch(GetProduct.getProductById({Product})))
   }
  


}
