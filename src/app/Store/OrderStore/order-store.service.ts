import { Injectable } from '@angular/core';
import { Address } from '../../../Interface/Address';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Product } from '../../../Interface/Product';
import { OrderService } from '../../Service/Order/order.service';
import { PaymentService } from '../../Service/Payment/payment.service';
import { Order } from '../../../Interface/Order';
import { CategoryAction } from '../../State/Cart/Cart.action';
import { CartStoreService } from '../CartStore/cart-store.service';
import { GetOrder } from '../../State/Order/Order.action';

@Injectable({
  providedIn: 'root'
})
export class OrderStoreService {
  private Address$: Observable<Address>;
  public currentCart$: Observable<Product[]>
  currentAddress: Address = {};
  Cart: Product[] = [];
  public currentAccount;
  status: "initial" | "uploading" | "success" | "fail" = "initial";
  public file: File | null = null; // Variable to store file
  public currentOrder: Order = {};



  constructor(private cartStore : CartStoreService ,private store:Store<{Address : Address,currentCart : Product[],Orders : Order[]}> ,private OrderService : OrderService ,private paymentService : PaymentService) {
    this.Address$ = this.store.select('Address');
    this.currentCart$ = this.store.select('currentCart');
    this.Address$.subscribe((data) => { this.currentAddress = data })
    this.currentCart$.subscribe((data) => { this.Cart = data })
    this.currentAccount = JSON.parse(localStorage.getItem('myAccount')!);

  }




  createOrder() {


    const data = { address : this.currentAddress ,orderItems : this.Cart ,customerId : this.currentAccount.account.customer.customer_id}
    this.cartStore.ClearCart()
      this.OrderService.CreateOrder(data).subscribe(data => {this.currentOrder = data})

  }
  AddPaymenImg() {
    const formData = new FormData();
    formData.append('file', this.file!, this.file!.name);
    formData.append('payment_type', 'QrCode');
    formData.append('payment_status', '');
    formData.append('payment_credit_name', '');
    formData.append('payment_credit_number', '');
    formData.append('payment_expiration_date', '');
    formData.append('payment_image', '');
    formData.append('orderId', JSON.stringify(this.currentOrder.order_id!));
    this.paymentService.CreatePayment(formData).subscribe((data) => { console.log(data) })

  }
  onChange(event: any) {
    const file: File = event.target.files[0];
    console.log(file)

    if (file) {
      this.status = "initial";
      this.file = file;
    }
  }





}
