import { Injectable } from '@angular/core';
import { Product } from '../../../Interface/Product';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthAction } from '../../State/Auth/Login.action'
import { AuthService } from '../../Service/Auth/Auth.service';
import { Account } from '../../../Interface/Account';



@Injectable({
  providedIn: 'root'
})
export class AuthStoreService {
  public currentCart$: Observable<Product[]>
  public CartList: Product[] = [];
  public Total = 0;
  public Account$: Observable<Account>
  public currentAccount = {};

  constructor(private store: Store<{ currentCart: Product[], Account: Account }>, private authService: AuthService) {
    this.currentCart$ = this.store.select('currentCart');
    this.Account$ = this.store.select('Account');
    this.Account$.subscribe((data) => this.currentAccount = data)

  }



  logout() {
    this.store.dispatch(AuthAction.logout({ Account: {} }))
  }
}
