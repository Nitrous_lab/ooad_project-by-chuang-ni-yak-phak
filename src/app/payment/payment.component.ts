import { Component } from '@angular/core';
import { CartStoreService } from '../Store/CartStore/cart-store.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrl: './payment.component.scss'
})
export class PaymentComponent {
  constructor(public cartStore : CartStoreService){}

}
