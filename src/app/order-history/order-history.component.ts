import { Component, Input } from '@angular/core';
import { Order } from '../../Interface/Order';
import { OrderStoreService } from '../Store/OrderStore/order-store.service';
import { Observable } from 'rxjs';
import { OrderService } from '../Service/Order/order.service';
import { Store } from '@ngrx/store';
import { GetOrder } from '../State/Order/Order.action';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrl: './order-history.component.scss'
})
export class OrderHistoryComponent {
  OrderList$: Observable<Order[]>
  Orders: Order[] = []
 public currentAccount ;
 


  constructor(private orderService: OrderService, private store: Store<{ Orders: Order[] }>) {
    this.OrderList$ = this.store.select('Orders')
    this.OrderList$.subscribe(data => this.Orders = data)
    this.currentAccount = JSON.parse(localStorage.getItem('myAccount')!);


  }
  ngOnInit(): void {
    this.orderService.GetAllOrder(this.currentAccount.account.customer.customer_id).subscribe(Orders => { this.store.dispatch(GetOrder.getSucces({ Orders })) });

  }


}
