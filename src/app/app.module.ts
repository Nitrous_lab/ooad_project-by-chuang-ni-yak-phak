import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MainPageComponent } from './main-page/main-page.component';
import { TabmenuComponent } from './tabmenu/tabmenu.component';
import { AllproductComponent } from './allproduct/allproduct.component';
import { ProfileComponent } from './profile/profile.component';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { ProductReducer, CurrentProductReducer } from './State/Product/Product.reducer';
import { RouterLink, RouterLinkActive, RouterOutlet, RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import { FavoriteComponent } from './favorite/favorite.component';
import { NavbarAdminComponent } from './navbar-admin/navbar-admin.component';
import { ProManageComponent } from './pro-manage/pro-manage.component';
import { CategoryReducer } from './State/Category/Category.reducer';
import { CheckOutComponent } from './check-out/check-out.component';
import { PaymentComponent } from './payment/payment.component';
import { cartReducer } from './State/Cart/Cart.reducer'
import { OrderHistoryComponent } from './order-history/order-history.component';
import { ItemCartComponent } from './item-cart/item-cart.component';
import { NavProfiileComponent } from './nav-profiile/nav-profiile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import { NabarReducer } from './State/Nabar/Nabar.reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountReducer } from './State/Auth/Login.reducer';
import { QrCodeComponent } from './qr-code/qr-code.component';
import { OrderManageComponent } from './order-manage/order-manage.component';
import { AwaitVerificationComponent } from './await-verification/await-verification.component';
import { addressReducer } from './State/Address/Address.reducer';
import { TaggingNumberComponent } from './tagging-number/tagging-number.component';
import { ModalTaggingComponent } from './modal-tagging/modal-tagging.component';
import { ShippingComponent } from './shipping/shipping.component';
import { DeliveredComponent } from './delivered/delivered.component';
import { StoreOrderHistoryComponent } from './store-order-history/store-order-history.component';
import { DeriveredHistoryComponent } from './derivered-history/derivered-history.component';
import { OrderCanceledComponent } from './order-canceled/order-canceled.component';
import { NabarAdminReducer } from './State/Nabar-Admin/NabarAdmin.reducer';
import { OrderReducer } from './State/Order/Order.reducer';
import { OrderCheckpaymentComponent } from './order-checkpayment/order-checkpayment.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainPageComponent,
    TabmenuComponent,
    AllproductComponent,
    ProfileComponent,
    ProfileComponent,
    ProductComponent,
    CartComponent,
    FavoriteComponent,
    NavbarAdminComponent,
    CheckOutComponent,
    PaymentComponent,
    OrderHistoryComponent,
    ItemCartComponent,
    NavProfiileComponent,
    LoginComponent,
    RegisterComponent,
    OrderStatusComponent,
    ProManageComponent,
    QrCodeComponent,
    OrderManageComponent,
    AwaitVerificationComponent,
    TaggingNumberComponent,
    ModalTaggingComponent,
    ModalTaggingComponent,
    ShippingComponent,
    DeliveredComponent,
    StoreOrderHistoryComponent,
    DeriveredHistoryComponent,
    OrderCanceledComponent,
    OrderCheckpaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({ Orders: OrderReducer ,Address: addressReducer, Account: AccountReducer, ProductList: ProductReducer, CategorysList: CategoryReducer, cuurentProduct: CurrentProductReducer, currentCart: cartReducer, Navbar: NabarReducer, NavbarAdmin: NabarAdminReducer }),
    RouterOutlet, RouterLink, RouterLinkActive, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
