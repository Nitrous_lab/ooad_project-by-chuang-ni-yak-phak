import { Component } from '@angular/core';
import { AuthStoreService } from '../Store/AuthStore/AuthStroe.service';

@Component({
  selector: 'app-navbar-admin',
  templateUrl: './navbar-admin.component.html',
  styleUrl: './navbar-admin.component.scss'
})
export class NavbarAdminComponent {
  constructor(public Auth : AuthStoreService){}

}
