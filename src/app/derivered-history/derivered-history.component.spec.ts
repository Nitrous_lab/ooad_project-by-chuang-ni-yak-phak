import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeriveredHistoryComponent } from './derivered-history.component';

describe('DeriveredHistoryComponent', () => {
  let component: DeriveredHistoryComponent;
  let fixture: ComponentFixture<DeriveredHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DeriveredHistoryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DeriveredHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
