import { Component } from '@angular/core';
import { CategoryStoreService } from '../Store/CategoryStore/category-store.service';
import { Store } from '@ngrx/store';
import { Category } from '../../Interface/Category';
import { Observable } from 'rxjs';
import { ProductStoreService } from '../Store/ProductStore/product-store.service';
import { Product } from '../../Interface/Product';
import { CartStoreService } from '../Store/CartStore/cart-store.service';
import { ActivatedRoute } from '@angular/router';
import { AuthStoreService } from '../Store/AuthStore/AuthStroe.service';
import { Account } from '../../Interface/Account';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {
 public CategorysList$ : Observable<Category[]>;
 public currentCart$ : Observable<Product[]>
 public Account$ : Observable<Account>;
 public currentAccount ;

  title = 'Navbar';
  constructor(public AuthStore : AuthStoreService,private CategoryStore : CategoryStoreService, private store : Store<{CategorysList : Category[], currentCart : Product[],Account : Account}>,public ProductStore : ProductStoreService ,public CartStore : CartStoreService){
    this.CategorysList$ = this.store.select('CategorysList')
    this.currentCart$ = this.store.select('currentCart')
    this.Account$ = this.store.select('Account')
    this.currentAccount = JSON.parse(localStorage.getItem('myAccount')!);
    // this.Account$.subscribe((data) => this.currentAccount = data);


    

  }
  ngOnInit(){
    this.CategoryStore.getCategory();

    console.log(this.currentAccount)


  }


}
