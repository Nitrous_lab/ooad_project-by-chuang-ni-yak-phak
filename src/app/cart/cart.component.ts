import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../../Interface/Product';
import { Store } from '@ngrx/store';
import { CartStoreService } from '../Store/CartStore/cart-store.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.scss'
})
export class CartComponent {
  public CartList$ : Observable<Product[]>
  constructor(private store : Store<{CartList : Product[]}>, public cartStore : CartStoreService){
    this.CartList$ = this.store.select('CartList');
  }

  

  
  

}
