import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavProfiileComponent } from './nav-profiile.component';

describe('NavProfiileComponent', () => {
  let component: NavProfiileComponent;
  let fixture: ComponentFixture<NavProfiileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavProfiileComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NavProfiileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
