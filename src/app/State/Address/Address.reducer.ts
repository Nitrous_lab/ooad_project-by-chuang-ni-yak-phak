import { createReducer, on } from '@ngrx/store';
import { Address } from '../../../Interface/Address';
import { AddressAction } from './Address.action';



export const initCart : Address = {
    address_id: 0,
    address_firstname: '',
    address_lastname: '',
    address_addDetail: '',
    address_district: '',
    address_province: '',
    address_postal_code: '',
    address_country: '',
    address_tel: ''
};

export const addressReducer = createReducer(initCart,
    on(AddressAction.addAddress,( state , {Address}) =>  {return state=Address}),
    on(AddressAction.removeAddress,( state , {Address}) =>  {return state = initCart}));