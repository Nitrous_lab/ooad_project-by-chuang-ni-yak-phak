import {  createActionGroup, props } from "@ngrx/store"; 
import { Address } from "../../../Interface/Address";

export const AddressAction = createActionGroup({source : 'Address API',events:{
    'Add Address': props<{Address : Address}>(),
    'Remove Address' :props<{Address : Address}>()

}})

