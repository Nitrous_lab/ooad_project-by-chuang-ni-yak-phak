import {  createActionGroup, props } from "@ngrx/store"; 
import { Product } from "../../../Interface/Product";

export const GetProduct = createActionGroup({source : 'Product API',
events:{'Get Succes': props<{Products : Product[]}>(),
         'Get ProductById' : props<{Product : Product}>()}});


         

