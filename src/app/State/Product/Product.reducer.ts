import { createReducer, on } from '@ngrx/store';
import { Product } from '../../../Interface/Product';
import { GetProduct } from '../Product/Product.action';


export const ProductList : Product[] = [] ;
export const currentProduct : Product = {};

export const ProductReducer = createReducer(ProductList,on(GetProduct.getSucces,( state , {Products}) =>  Products));

export const CurrentProductReducer = createReducer(currentProduct,on(GetProduct.getProductById,( state , {Product}) =>  Product));
