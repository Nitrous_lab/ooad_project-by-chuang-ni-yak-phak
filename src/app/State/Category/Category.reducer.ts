import { createReducer, on } from '@ngrx/store';
import { Category } from '../../../Interface/Category';
import { GetCategory } from '../Category/Category.action';


export const init : Category[] = [] ;

export const CategoryReducer = createReducer(init,on(GetCategory.getSucces,( state , {Categorys}) =>  Categorys));