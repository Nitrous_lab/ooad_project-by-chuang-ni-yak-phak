import {  createActionGroup, props } from "@ngrx/store"; 
import { Account } from "../../../Interface/Account";

export const AuthAction = createActionGroup({source : 'Auth API',events:{
    'Login': props<{Account : Account}>(),
    'Logout' :props<{Account : Account}>()

}})

