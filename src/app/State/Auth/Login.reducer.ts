import { createReducer, on } from '@ngrx/store';

import { AuthAction } from '../Auth/Login.action';
import { Account } from '../../../Interface/Account';


export const init  = {account:{}} ;

export const AccountReducer = createReducer(init,on(AuthAction.login,( state , {Account}) =>  {
    localStorage.setItem('myAccount',JSON.stringify(Account));
    
    return {...state  ,... Account}})
    ,on(AuthAction.logout,(state ) => {
        localStorage.removeItem('myAccount');
        state = init
        return state})

);