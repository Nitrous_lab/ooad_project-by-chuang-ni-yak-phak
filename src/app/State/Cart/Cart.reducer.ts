import { createReducer, on } from '@ngrx/store';
import { Product } from '../../../Interface/Product';
import { CategoryAction } from '../Cart/Cart.action';



export const initCart : Product[] = [] ;

export const cartReducer = createReducer(initCart,
    on(CategoryAction.addProduct,( state , {Product}) =>  {
        
        // if (state.indexOf(Product) > -1) return state ;
        return [...state , Product]}),
    on(CategoryAction.removeProduct,( state , {Product}) =>  state.filter((item) => item !== Product) ),on(CategoryAction.clean,(state ,{Product}) => {
        return initCart;
    }));