import {  createActionGroup, props } from "@ngrx/store"; 
import { Product } from "../../../Interface/Product";

export const CategoryAction = createActionGroup({source : 'Category API',events:{
    'Add Product': props<{Product : Product}>(),
    'Remove Product' :props<{Product : Product}>(),
    'Clean' : props<{Product : Product}>()

}})

