import { createReducer, on } from '@ngrx/store';
import { NavbarDisable, NavbarEnable } from '../Nabar/Nabar.action';


export const init: Boolean = false;

export const NabarReducer = createReducer(
  init,
  on(NavbarDisable, (state) => { return state = false }),
  on(NavbarEnable, (state) => { return state = true })
);