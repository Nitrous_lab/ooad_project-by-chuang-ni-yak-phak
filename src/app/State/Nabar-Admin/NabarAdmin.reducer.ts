import { createReducer, on } from '@ngrx/store';
import { NavbarAdminDisable, NavbarAdminEnable } from './NabarAdmin.action';


export const init: Boolean = false;

export const NabarAdminReducer = createReducer(
  init,
  on(NavbarAdminDisable, (state) => { return state = false }),
  on(NavbarAdminEnable, (state) => { return state = true })
);