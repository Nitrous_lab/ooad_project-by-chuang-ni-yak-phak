import {  createActionGroup, props } from "@ngrx/store"; 
import { Order } from "../../../Interface/Order";

export const GetOrder = createActionGroup({source : 'Order API',events:{'Get Succes': props<{Orders : Order[]}>()}})

