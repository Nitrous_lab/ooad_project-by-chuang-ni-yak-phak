import { Component,  } from '@angular/core';
import { Product } from '../../Interface/Product';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ProductStoreService } from '../Store/ProductStore/product-store.service';
import { CartStoreService } from '../Store/CartStore/cart-store.service';
import { ActivatedRoute } from '@angular/router';
import { http_server_image } from '../../../HTTP-Image'; 
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductService } from '../Service/Product/product.service';
import { GetProduct } from '../State/Product/Product.action';
import { CategoryAction } from '../State/Cart/Cart.action';




@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.scss'
})
export class ProductComponent {
  public cuurentProduct$ : Observable<Product> ;
  public Product : Product = {};
  productId ;
  public http = http_server_image;
  item: FormGroup;




  constructor(private ProductService : ProductService,private formBuilder: FormBuilder,private store:Store<{cuurentProduct : Product}>,private ProductStore : ProductStoreService,public cartStore : CartStoreService,private activatedRoute : ActivatedRoute,public sanitizer: DomSanitizer) {
    this.productId = this.activatedRoute.snapshot.paramMap.get('id');
    this.cuurentProduct$ = this.store.select('cuurentProduct');
  this.ProductService.getProductById(parseInt(this.productId!)).subscribe(Product => {this.store.dispatch(GetProduct.getProductById({Product}))})
    
    this.item = this.formBuilder.group(
      {
        product_id: [],
        product_name: [''],
        product_size: [[]],
        product_price: [],
        product_color: [''],
        product_description: [],
        product_amout: [1],
        product_categoryId: [],
        product_img_url: [],
        createdAt: [],
        updatedAt: [],
        deletedAt: []
      },
    );
    }


 ngOnInit(){
  this.ProductService.getProductById(parseInt(this.productId!)).subscribe(Product => {this.store.dispatch(GetProduct.getProductById({Product}))
  this.Product = Product;
  this.item.patchValue({        
    product_id: Product.product_id,
    product_name: Product.product_name,
    product_price: Product.product_price,
    product_description: Product.product_description,
    product_categoryId: Product.product_categoryId,
    product_img_url: Product.product_img_url,
    createdAt: Product.createdAt,
    updatedAt: Product.updatedAt,
    deletedAt: Product.deletedAt})
  console.log(this.Product)
  console.log(this.item.value)}
  )}

  sizeSelect(size : String){
    this.item.patchValue({product_size : [size]})
    console.log(this.item.value)



  }




}
