import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, map } from 'rxjs';
import { Product } from '../Interface/Product';
import { ActivatedRoute, Data } from '@angular/router';
import { Account } from '../Interface/Account';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',


})
export class AppComponent {
  title = 'ooad-project';
  Navbar$: Observable<Boolean>;
  NavbarAdmin$: Observable<Boolean>;
  currentUser: Account | null = null;


  getCurrentUser(): Account | null {
    return this.currentUser;
  }

  constructor(public store: Store<{ Navbar: Boolean, NavbarAdmin: Boolean }>) {
    this.Navbar$ = this.store.select('Navbar')
    this.NavbarAdmin$ = this.store.select('NavbarAdmin')
  }

  ngOnInit(
  ) {

  }


}
