import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ProfileComponent } from './profile/profile.component';
import { AllproductComponent } from './allproduct/allproduct.component';
import { CartComponent } from './cart/cart.component';
import { FavoriteComponent } from './favorite/favorite.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { PaymentComponent } from './payment/payment.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { ItemCartComponent } from './item-cart/item-cart.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import { AppComponent } from './app.component';
import { QrCodeComponent } from './qr-code/qr-code.component';
import { authGuard, NavbarAdmin, NavbarCustomer } from './auth.guard';
import { OrderManageComponent } from './order-manage/order-manage.component';
import { ProManageComponent } from './pro-manage/pro-manage.component';
import { StoreOrderHistoryComponent } from './store-order-history/store-order-history.component';
import { NavbarAdminComponent } from './navbar-admin/navbar-admin.component';


const routes: Routes = [
  { path: 'product/:id', component: ProductComponent, canActivate: [NavbarCustomer] },
  { path: 'category/:id/product/:id', component: ProductComponent, canActivate: [NavbarCustomer] },
  { path: 'Main', component: MainPageComponent, canActivate: [NavbarCustomer] },
  { path: 'profile', component: ProfileComponent, canActivate: [authGuard, NavbarCustomer] },
  { path: '', component: MainPageComponent, canActivate: [NavbarCustomer] },
  { path: 'category/:id', component: AllproductComponent, canActivate: [NavbarCustomer] },
  { path: 'category', component: AllproductComponent, canActivate: [NavbarCustomer] },
  { path: 'cart', component: CartComponent, canActivate: [NavbarCustomer] },
  { path: 'fav', component: FavoriteComponent, canActivate: [authGuard, NavbarCustomer] },
  { path: 'checkout', component: CheckOutComponent, canActivate: [authGuard, NavbarCustomer] },
  { path: 'payment', component: PaymentComponent, canActivate: [authGuard, NavbarCustomer] },
  { path: 'order-history', component: OrderHistoryComponent, canActivate: [authGuard, NavbarCustomer] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'order-status/:id', component: OrderStatusComponent, canActivate: [authGuard, NavbarCustomer] },
  { path: 'order-manage', component: OrderManageComponent, canActivate: [NavbarAdmin] },
  { path: 'product-manage', canActivate: [authGuard, NavbarAdmin], component: ProManageComponent },
  { path: 'store-history', component: StoreOrderHistoryComponent }
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
