import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductStoreService } from '../Store/ProductStore/product-store.service';
import { Product } from '../../Interface/Product';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { http_server_image } from '../../../HTTP-Image'; 
import { Category } from '../../Interface/Category';

@Component({
  selector: 'app-allproduct',
  templateUrl: './allproduct.component.html',
  styleUrl: './allproduct.component.scss'
})
export class AllproductComponent {
  public CategorysList$ : Observable<Category[]>
  categoryId ;
  ProductList$ : Observable<Product[]>
  public CategoryList : Category[] = []
  public http = http_server_image;
  constructor(private activatedRoute : ActivatedRoute, public ProductStore : ProductStoreService, private store : Store<{ProductList : Product[],CategorysList : Category[]}>){
    this.categoryId = this.activatedRoute.snapshot.paramMap.get('id')
    this.ProductList$ = this.store.select('ProductList')
    this.CategorysList$ = this.store.select('CategorysList')

  }
  ngOnInit(): void {
    this.CategorysList$.subscribe((data) => {
      this.CategoryList = data
    })
    this.ProductStore.getProductByCategoryId(parseInt(this.categoryId!))
    console.log(this.ProductList$)
    
  }


}
