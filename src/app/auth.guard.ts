import { inject } from '@angular/core';
import { Router } from '@angular/router';

import { AuthStoreService } from './Store/AuthStore/AuthStroe.service';
import { Store } from '@ngrx/store';
import { NavbarDisable, NavbarEnable } from './State/Nabar/Nabar.action';
import { NavbarAdminDisable, NavbarAdminEnable } from './State/Nabar-Admin/NabarAdmin.action'

export const authGuard = () => {
  const router = inject(Router);
  const authStore = inject(AuthStoreService);

  
  const account  = JSON.parse(localStorage.getItem('myAccount')!);
  console.log(account)
  if (account) {
    if (account.account.user_role == 'Admin') {
      router.parseUrl('/product-manage');
      return true;
      // } else {
      //   console.log("User");
      //   return router.parseUrl('/Main');
      // }
    }else if (account.account.user_role == 'customer') {
      router.parseUrl('/Main');
      return true;
    }
  }
  return router.parseUrl('/login');
};
export const NavbarCustomer = () => {
  const store = inject(Store)
  store.dispatch(NavbarAdminDisable())
  store.dispatch(NavbarEnable())
  return true

}

export const NavbarAdmin = () => {
  const store = inject(Store)
  store.dispatch(NavbarAdminEnable())
  store.dispatch(NavbarDisable())
  return true

}