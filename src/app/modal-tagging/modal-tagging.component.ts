import { Component, Input } from '@angular/core';
import { Order } from '../../Interface/Order';
import { FormControl } from '@angular/forms';
import { ShipmentService } from '../Service/Shipment/shipment.service';
import { OrderStoreService } from '../Store/OrderStore/order-store.service';
import { OrderService } from '../Service/Order/order.service';
import { Store } from '@ngrx/store';
import { GetOrder } from '../State/Order/Order.action';

@Component({
  selector: 'app-modal-tagging',
  templateUrl: './modal-tagging.component.html',
  styleUrl: './modal-tagging.component.scss'
})
export class ModalTaggingComponent {
  shipment = ['EMS Thailand','Kerry','Flash','J&T']
  trackingNo = new FormControl('')
  @Input() item : Order = {}
  type = 'EMS Thailand'
  constructor(private shipmentService : ShipmentService ,public OrderService : OrderService,private store : Store<{Orders : Order[]}>){}

  callType(value: any) {
    this.type = value
    console.log(value);
    
  }
  OrderAddTrackingNumber(){
    const data = {shipment_tracking_no : this.trackingNo.value , shipment_type : this.type , order_id: this.item.order_id}
    console.log(data)
    this.shipmentService.CreateShipment(data).subscribe(data => {console.log(data);this.GetOrderInShipment();})
    

  }

  GetOrderInShipment(){
    this.OrderService.OrderInShipment().subscribe(Orders  => { this.store.dispatch(GetOrder.getSucces({Orders}))})
  }


}
