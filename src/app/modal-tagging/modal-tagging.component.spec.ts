import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTaggingComponent } from './modal-tagging.component';

describe('ModalTaggingComponent', () => {
  let component: ModalTaggingComponent;
  let fixture: ComponentFixture<ModalTaggingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalTaggingComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalTaggingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
