import { Component } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { NavbarDisable, NavbarEnable } from '../State/Nabar/Nabar.action';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthStoreService } from '../Store/AuthStore/AuthStroe.service';
import { Account } from '../../Interface/Account';
import { Location } from '@angular/common';
import { AuthAction } from '../State/Auth/Login.action';
import { AuthService } from '../Service/Auth/Auth.service';
import { NavbarAdminDisable, NavbarAdminEnable } from '../State/Nabar-Admin/NabarAdmin.action';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  registerForm: FormGroup;
  submitted = false;
  public Account$: Observable<Account>
  currentAccount: Account = {};



  constructor(private location: Location, public store: Store<{ Account: Account }>, private AuthStore: AuthStoreService, private formBuilder: FormBuilder, private authService: AuthService, private Router: Router) {
    this.Account$ = this.store.select('Account');
    this.Account$.subscribe((data) => this.currentAccount = data)
    this.currentAccount = JSON.parse(localStorage.getItem('myAccount')!);
    this.store.dispatch(NavbarDisable())
    this.store.dispatch(NavbarAdminDisable())

    this.registerForm = this.formBuilder.group(
      {
        user_name: ['', [Validators.required], Validators.minLength(5), Validators.maxLength(128)],
        user_password: [''],
      },
    );

  }


  ngOnInit() {
    // this.Account$.subscribe((data) => this.AuthStore.currentAccount = data)
    console.log(this.currentAccount)


  }
  // ngOnDestroy() {
  //   this.store.dispatch(NavbarEnable())


  // }
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.authService.getAccount(this.registerForm.value).subscribe({next : data => {
      this.store.dispatch(AuthAction.login({ Account : data }));
      console.log(data)
    this.submitted = true;
      if (!data) {
      return;
    }else if (data.account.user_role == 'Admin'){
      this.onReset()
       this.Router.navigate(['/product-manage']);

    }else if (data.account.user_role == 'customer') {
      this.onReset()
       this.Router.navigate(['/Main']);

    }
      
    },
      error: err => {
        alert("Email Or Password is Worng")
      }

    })



  }
  // onSubmit() {
  //   this.submitted = true;

  //   if (this.registerForm.invalid) {
  //     return;
  //   }
  // Account => {
  //   this.store.dispatch(AuthAction.login({ Account }));
  //   console.log(Account)
  //   this.submitted = true;
    
  //   if (this.registerForm.invalid || !Account) {
  //     return;
  //   }
  //   // return this.location.back();

  //   else if (Account.account.user_role == 'Admin') {
  //     this.onReset();
  //     this.store.dispatch(NavbarDisable())
  //     this.store.dispatch(NavbarAdminEnable())
  //     return this.Router.navigate(['/product-manage']);

  //     // } else {
  //     //   console.log("User");
  //     //   return router.parseUrl('/Main');
  //     // }
  //   } else if (Account.account.user_role == 'Dev') {
  //     this.onReset();
  //     this.store.dispatch(NavbarAdminDisable())
  //     this.store.dispatch(NavbarEnable())

  //   }
  //   return this.Router.navigate(['/Main']);




  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

}
