import { Component, Input } from '@angular/core';
import { Product } from '../../Interface/Product';
import { CartStoreService } from '../Store/CartStore/cart-store.service';

@Component({
  selector: 'app-item-cart',
  templateUrl: './item-cart.component.html',
  styleUrl: './item-cart.component.scss'
})
export class ItemCartComponent {
  constructor(public cartStore : CartStoreService){}
  @Input() item : Product = {};

}
