import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCheckpaymentComponent } from './order-checkpayment.component';

describe('OrderCheckpaymentComponent', () => {
  let component: OrderCheckpaymentComponent;
  let fixture: ComponentFixture<OrderCheckpaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OrderCheckpaymentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OrderCheckpaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
