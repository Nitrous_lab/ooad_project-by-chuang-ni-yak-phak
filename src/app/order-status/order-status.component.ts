import { Component, Input } from '@angular/core';
import { Order } from '../../Interface/Order';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../Service/Order/order.service';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.component.html',
  styleUrl: './order-status.component.scss'
})
export class OrderStatusComponent {
  OrderId;
  Order : Order = {}
  constructor(private activatedRoute : ActivatedRoute,private OrderService : OrderService){
    this.OrderId = this.activatedRoute.snapshot.paramMap.get('id');
    this.OrderService.GetById(parseInt(this.OrderId!)).subscribe(data => this.Order = data)
  }

}
