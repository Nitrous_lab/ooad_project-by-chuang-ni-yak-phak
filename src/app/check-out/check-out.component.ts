import { Component } from '@angular/core';
import { CartStoreService } from '../Store/CartStore/cart-store.service';
import { Product } from '../../Interface/Product';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Account } from '../../Interface/Account';
import { AuthStoreService } from '../Store/AuthStore/AuthStroe.service';
import { Location } from '@angular/common';
import { AuthAction } from '../State/Auth/Login.action';
import { AuthService } from '../Service/Auth/Auth.service';
import { Observable } from 'rxjs';
import { Address } from '../../Interface/Address';
import { AddressAction } from '../State/Address/Address.action';
import { Router } from '@angular/router';
import { OrderStoreService } from '../Store/OrderStore/order-store.service';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrl: './check-out.component.scss'
})
export class CheckOutComponent {
  checkoutForm: FormGroup;
  submitted = false;
  currentAccount: Account = {};



  constructor(private OrderStore : OrderStoreService, private store: Store<{ CartList: Product[] , Address: Address}>, public cartStore: CartStoreService, private formBuilder: FormBuilder, private authService: AuthService, private Router: Router) {
    this.checkoutForm = this.formBuilder.group({
      // address_id: [],
      // address_firstname: [],
      // address_lastname: [],
      // address_addDetail: [],
      // address_district: [],
      // address_province: [],
      // address_postal_code: [],
      // address_country: [],
      // address_tel: []
      address_id: [0],
      address_firstname : ['', [Validators.required]],
      address_lastname : ['', [Validators.required]],
      address_addDetail : ['', [Validators.required]],
      address_district : ['', [Validators.required]],
      address_province : ['', [Validators.required]],
      address_postal_code : ['', [Validators.required]],
      address_country : ['', [Validators.required]],
      address_tel : ['', [Validators.required]]
      
      // address_id: ['', [Validators.required]],
      // address_firstname : ['', [Validators.required], Validators.minLength(1)],
      // address_lastname : ['', [Validators.required], Validators.minLength(1)],
      // address_addDetail : ['', [Validators.required], Validators.minLength(1), Validators.maxLength(300)],
      // address_district : ['', [Validators.required], Validators.minLength(1)],
      // address_province : ['', [Validators.required], Validators.minLength(3)],
      // address_postal_code : ['', [Validators.required], Validators.minLength(5), [Validators.required, Validators.pattern("^[0-9]*$")]],
      // address_country : ['', [Validators.required], Validators.minLength(1)],
      // address_tel : ['', [Validators.required], Validators.minLength(10), [Validators.required, Validators.pattern("^[0-9]*$")]]
    },
    );
  }

  onSubmit() {
    this.submitted = true;
    if (this.checkoutForm.invalid) {
        alert("Please fill in complete information.")
        return;
    }
    console.log(this.checkoutForm);
    this.store.dispatch(AddressAction.addAddress({ Address: this.checkoutForm.value }));
    this.Router.navigateByUrl("/payment");
}






}
