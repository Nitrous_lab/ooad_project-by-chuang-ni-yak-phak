import { Component, Input, input } from '@angular/core';
import { Order, OrderItem } from '../../Interface/Order';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { GetOrder } from '../State/Order/Order.action';
import { OrderService } from '../Service/Order/order.service';
import { OrderStoreService } from '../Store/OrderStore/order-store.service';

@Component({
  selector: 'app-await-verification',
  templateUrl: './await-verification.component.html',
  styleUrl: './await-verification.component.scss'
})
export class AwaitVerificationComponent {

  
  OrderList$: Observable<Order[]>
  Orders : Order[] = []
  CurrentOrder : Order = {}

  constructor(public OrderService : OrderService,private store: Store<{ Orders: Order[] }> ,private orderService : OrderService) {
    this.OrderList$ = this.store.select('Orders')
    this.OrderList$.subscribe(data => this.Orders = data)
  }

  onmodal(OrderItem: Order) {
    this.CurrentOrder = OrderItem
  }
  
  onAcceptOrder(id :number){
    this.PatchOrderVerify(id)
    this.OrderService.GetAllInOrder().subscribe(Orders => {this.store.dispatch(GetOrder.getSucces({Orders}))});
    this.OrderList$.subscribe(data => console.log(data))

    
  }

  ngOnInit(): void {
    this.GetAllInOrder()
    
  }
  PatchOrderVerify(id : number){
    this.OrderService.PatchOrderVerify(id).subscribe(data => {console.log(data);this.GetAllInOrder()})
  }
  GetAllInOrder(){
    this.OrderService.GetAllInOrder().subscribe(Orders => {this.store.dispatch(GetOrder.getSucces({Orders}))});
  }


}
