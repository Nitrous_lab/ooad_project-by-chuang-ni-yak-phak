import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AwaitVerificationComponent } from './await-verification.component';

describe('AwaitVerificationComponent', () => {
  let component: AwaitVerificationComponent;
  let fixture: ComponentFixture<AwaitVerificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AwaitVerificationComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AwaitVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
