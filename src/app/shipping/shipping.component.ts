import { Component } from '@angular/core';
import { Order } from '../../Interface/Order';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { OrderStoreService } from '../Store/OrderStore/order-store.service';
import { OrderService } from '../Service/Order/order.service';
import { GetOrder } from '../State/Order/Order.action';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrl: './shipping.component.scss'
})
export class ShippingComponent {
  OrderList$ : Observable<Order[]>

  constructor(private store : Store<{Orders : Order[]}>,private OrderService :OrderService){
  this.OrderList$ = this.store.select('Orders')

}
ngOnInit(): void {
  this.GetOnShipment()
  
}
GetOnShipment(){
  this.OrderService.OrderOnShipment().subscribe(Orders => {this.store.dispatch(GetOrder.getSucces({Orders}))})
}

}
