import { Component } from '@angular/core';
import { Order } from '../../Interface/Order';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { OrderService } from '../Service/Order/order.service';
import { OrderStoreService } from '../Store/OrderStore/order-store.service';
import { GetOrder } from '../State/Order/Order.action';


@Component({
  selector: 'app-order-manage',
  templateUrl: './order-manage.component.html',
  styleUrl: './order-manage.component.scss'
})
export class OrderManageComponent {
  
  OrderList$ : Observable<Order[]>
  Orders : Order[] = []
  constructor(public OrderService : OrderService, private store : Store<{Orders : Order[]}>) {
    this.OrderList$ = this.store.select('Orders')

  

  }

  ngOnInit(): void {
    this.GetAllInOrder()
    
  }

  GetAllInOrder(){
    this.OrderService.GetAllInOrder().subscribe(Orders => {this.store.dispatch(GetOrder.getSucces({Orders}))});
  }
  GetOrderInShipment(){
    this.OrderService.OrderInShipment().subscribe(Orders  => { this.store.dispatch(GetOrder.getSucces({Orders}))})
  }
  GetOnShipment(){
    this.OrderService.OrderOnShipment().subscribe(Orders => {this.store.dispatch(GetOrder.getSucces({Orders}))})
  }

  PatchOrderVerify(id : number){
    this.OrderService.PatchOrderVerify(id).subscribe(data => console.log(data))
  }

}
