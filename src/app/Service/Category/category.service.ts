import { Injectable } from '@angular/core';
import { Category } from '../../../Interface/Category';
import { map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http : HttpClient) { }
  getCategory() {
    return  this.http.get<Category[]>('http://localhost:3000/category/').pipe(map((Categorys) => Categorys || []));
   }
}
