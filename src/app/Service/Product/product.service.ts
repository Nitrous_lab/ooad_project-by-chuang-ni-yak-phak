
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../../../Interface/Product';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  constructor(private http : HttpClient) { }
    getProduct() {
     return  this.http.get<Product[]>('http://localhost:3000/product/').pipe(map((Products) => Products || []));
    }

    getProductByCategory(id : number) {
      return  this.http.get<Product[]>(`http://localhost:3000/product/category/${id}`).pipe(map((Products) => Products || []));
     }

     getProductById(id : number) {
      return  this.http.get<Product>(`http://localhost:3000/product/${id}`).pipe(map((Product) => Product || {}));
     }
}
