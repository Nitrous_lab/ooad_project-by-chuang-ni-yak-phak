import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../Auth/Auth.service';
import { inject } from '@angular/core';


export function adminGuard(authService: AuthService, router: Router) {
  return () => {
    if (authService.currentUser?.user_role === 'Admin') {
      return router.navigate(['/product-manage']);
    } else {
      router.navigate(['/main']);
      return false;
    }
  };
}

