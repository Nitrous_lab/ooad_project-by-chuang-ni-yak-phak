import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http : HttpClient) { }
  CreatePayment(data :FormData) {
    return this.http.post<any>('http://localhost:3000/payment/',data);
   }
}
