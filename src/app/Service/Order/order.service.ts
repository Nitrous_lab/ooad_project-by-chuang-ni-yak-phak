import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http : HttpClient) { 
    
  }
  CreateOrder(data :any) {
    
    return this.http.post<any>('http://localhost:3000/order/',data
  );
   }

   GetAllInOrder() {
    
    return this.http.get<any>('http://localhost:3000/order/payment/order');
   }

   GetAllOrder(id : number) {
    return this.http.get<any>(`http://localhost:3000/order/${id}/customer`);
   }
   GetById(id : number) {
    return this.http.get<any>(`http://localhost:3000/order/${id}`);
   }

   PatchOrderVerify(id :number){
    return this.http.patch<any>(`http://localhost:3000/order/${id}/verify`,{})
   }

   OrderInShipment(){
    return this.http.get<any>('http://localhost:3000/order/order/sp')
   }
   OrderOnShipment(){
    return this.http.get<any>('http://localhost:3000/order/OnShipment/order')
   }

}
