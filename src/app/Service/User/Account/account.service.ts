import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private apiUrl = 'your_api_url';

  constructor(private http: HttpClient) {}

  registerUser(email: string, firstName: string, lastName: string,  password: string): Observable<any> {
    const userData = {email, firstName, lastName, password };
    return this.http.post<any>(`${this.apiUrl}/register`, userData);
  }
}