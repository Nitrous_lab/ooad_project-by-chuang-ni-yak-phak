import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../../Interface/User';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http : HttpClient) { }
  getUser() {
    return  this.http.get<User[]>('http://localhost:3000/customer/').pipe(map((Customer) => Customer || []));
   }
}
