import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Account } from '../../../Interface/Account';
import { User } from '../../../Interface/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser: Account | null = null;

  getCurrentUser(): Account | null {
    return this.currentUser;
  }

  constructor(private http: HttpClient) { }
  getAccount(data: any) {
    return this.http.post<any>('http://localhost:3000/auth/', data);
  }
}
