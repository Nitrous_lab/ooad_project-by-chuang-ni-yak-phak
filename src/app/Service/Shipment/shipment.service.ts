import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShipmentService {

  constructor(private http : HttpClient) { }
  
  CreateShipment(data : any){
    return this.http.post<any>('http://localhost:3000/shipment/',data);
  }
}
