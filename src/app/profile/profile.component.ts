import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { NavbarDisable, NavbarEnable } from '../State/Nabar/Nabar.action';
import { NavbarAdminDisable } from '../State/Nabar-Admin/NabarAdmin.action';
import { AuthService } from '../Service/Auth/Auth.service';
import { Account } from '../../Interface/Account';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.scss'
})
export class ProfileComponent {
  constructor(public store: Store<{ Account: Account }>, private authService: AuthService) {
    // this.store.dispatch(NavbarAdminDisable())
    // this.store.dispatch(NavbarEnable())

  }
  ngOnInit(): void {


    const account = JSON.parse(localStorage.getItem('myAccount')!);
    console.log(account)
  }

}
