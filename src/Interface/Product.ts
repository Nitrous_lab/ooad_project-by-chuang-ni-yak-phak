export interface Product {
  product_id?: number;
  product_name?: string;
  product_size?: string[];
  product_price?: number;
  product_color?: string;
  product_description?: string;
  product_amout?: number;
  product_categoryId?: number;
  product_img_url?: string[];
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: any;
}


