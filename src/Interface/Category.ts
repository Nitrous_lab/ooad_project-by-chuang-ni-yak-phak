export interface Category {
    category_id: number;
    category_name: string;
    createdAt: string;
    updatedAt: string;
    deletedAt?: any;
}
