export interface User {
    customer_id: number;
    customer_name: string;
    email: string;
    password: string;
    tel: string;
    updatedAt: Date;
    deletedAt: Date;
}
