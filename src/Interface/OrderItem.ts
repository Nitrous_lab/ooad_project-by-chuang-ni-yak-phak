export interface OrderItem {
    productId: number;
    amount: number;
    size: string;
  
}