

export interface Account {
  account_id?: number;
  user_name?: string;
  user_password?: string;
  user_role?: string;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: any;
}