export interface Order {
  order_id?: number;
  order_total_price?: number;
  order_sum_amount?: number;
  order_status?: string;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: any;
  orderItems?: OrderItem[];
  customer?: Customer;
  payment?: Payment;
  address?: Address;
  shipment?: Shipment;


}

export interface Payment {
  payment_id?: number;
  payment_type?: string;
  payment_status?: string;
  payment_credit_name?: any;
  payment_credit_number?: any;
  payment_expiration_date?: any;
  payment_image?: string;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: any;
}

 interface Customer {
  customer_id?: number;
  customer_name?: string;
  email?: string;
  tel?: string;
}

export interface OrderItem {
  order_detail_id?: number;
  name?: string;
  price?: number;
  amount?: number;
  total?: number;
  product_img_url?: string[];
  product_size?: string;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: any;
}



 interface Address {
  address_id?: number;
  address_firstname?: string;
  address_lastname?: string;
  address_addDetail?: string;
  address_district?: string;
  address_province?: string;
  address_postal_code?: string;
  address_country?: string;
  address_tel?: string;
}


interface Shipment {
  shipment_id?: number;
  shipment_tracking_no?: string;
  shipment_status?: string;
  shipment_type?: string;
  shipment_cost?: number;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: any;
}