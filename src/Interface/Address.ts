export interface Address {
    address_id?: number;
    address_firstname?: string;
    address_lastname?: string;
    address_addDetail?: string;
    address_district?: string;
    address_province?: string;
    address_postal_code?: string;
    address_country?: string;
    address_tel?: string;
  }